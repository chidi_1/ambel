/**
 * appName - http://chidi-frontend.esy.es/
 * @version v0.1.0
 * @author bev-olga@yandex.ru
 */
(function() {


}).call(this);

(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);

/*! device.js 0.1.57 */
(function(){var a,b,c,d,e,f,g,h,i;window.device={},b=window.document.documentElement,i=window.navigator.userAgent.toLowerCase(),device.ios=function(){return device.iphone()||device.ipod()||device.ipad()},device.iphone=function(){return c("iphone")},device.ipod=function(){return c("ipod")},device.ipad=function(){return c("ipad")},device.android=function(){return c("android")},device.androidPhone=function(){return device.android()&&c("mobile")},device.androidTablet=function(){return device.android()&&!c("mobile")},device.blackberry=function(){return c("blackberry")||c("bb10")||c("rim")},device.blackberryPhone=function(){return device.blackberry()&&!c("tablet")},device.blackberryTablet=function(){return device.blackberry()&&c("tablet")},device.windows=function(){return c("windows")},device.windowsPhone=function(){return device.windows()&&c("phone")},device.windowsTablet=function(){return device.windows()&&c("touch")},device.fxos=function(){return c("(mobile; rv:")||c("(tablet; rv:")},device.fxosPhone=function(){return device.fxos()&&c("mobile")},device.fxosTablet=function(){return device.fxos()&&c("tablet")},device.mobile=function(){return device.androidPhone()||device.iphone()||device.ipod()||device.windowsPhone()||device.blackberryPhone()||device.fxosPhone()},device.tablet=function(){return device.ipad()||device.androidTablet()||device.blackberryTablet()||device.windowsTablet()||device.fxosTablet()},device.portrait=function(){return 90!==Math.abs(window.orientation)},device.landscape=function(){return 90===Math.abs(window.orientation)},c=function(a){return-1!==i.indexOf(a)},e=function(a){var c;return c=new RegExp(a,"i"),b.className.match(c)},a=function(a){return e(a)?void 0:b.className+=" "+a},g=function(a){return e(a)?b.className=b.className.replace(a,""):void 0},device.ios()?device.ipad()?a("ios ipad tablet"):device.iphone()?a("ios iphone mobile"):device.ipod()&&a("ios ipod mobile"):device.android()?device.androidTablet()?a("android tablet"):a("android mobile"):device.blackberry()?device.blackberryTablet()?a("blackberry tablet"):a("blackberry mobile"):device.windows()?device.windowsTablet()?a("windows tablet"):device.windowsPhone()?a("windows mobile"):a("desktop"):device.fxos()?device.fxosTablet()?a("fxos tablet"):a("fxos mobile"):a("desktop"),d=function(){return device.landscape()?(g("portrait"),a("landscape")):(g("landscape"),a("portrait"))},h="onorientationchange"in window,f=h?"orientationchange":"resize",window.addEventListener?window.addEventListener(f,d,!1):window.attachEvent?window.attachEvent(f,d):window[f]=d,d()}).call(this);

$(document).ready(function(){
     var timeout_link;

    // стилизация input file
    if($('.js--file-styled').length){
        $('.js--file-styled').each(function(){
          $(this).styler({});
        })
    }

    // селект
    if($('.js--select-styled').length){
        $('.js--select-styled').each(function(){
          $(this).styler({});
        })
    }
    $(document).on('click', '.js--img-load', function (evt){
        evt.preventDefault();
        $(this).closest('.js--img--container').removeClass('error').find('.input-file').trigger('click');
    });

    $(document).on('change', '.js--img--container .input-file', function (evt){
        var block = $(this).parents('.js--img--container');
        var flag;
        if(block.hasClass('add-img')){
            flag = true;
        }else{
            flag = false;
        }

        readURL(this, block, flag);

        return false;
    });

    $(document).on('click', '.project__about-images li', function(){
        return false;
    })

     function readURL(input, container, flag) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if(flag === false){
                    container.find('.js--img--container-img').attr('src', e.target.result);
                }
                else{
                    container.prev('.project__about-images').append('<li>' +
                    //'<a href="' + e.target.result + '" class="fancy">' +
                    '<img src="' + e.target.result + '" alt />' +
                    //'</a>' +
                    '</li>');

                    container.find('.input-file').removeClass('input-file');
                    container.append('<input type="file" name="file[]" class="input-file">');
                }
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('click', '.js--form-send', function(){
        var form = $(this).closest('form');
        if(form.find('.inp').prop('value') != ''){
            form.trigger('submit');
        }
        return false;
    })

    if($('.fancy').length) {
        $('.fancy').each(function(){
          $(this).fancybox();
        })
    }

    if($('.inp-phone').length) {
        $('.inp-phone').each(function(){
          $(this).mask('+7(999)999-99-99');
        })
    }

    if($('.inp-call').length) {
        $('.inp-call').each(function(){
          $(this).mask('99/99/9999');
        })
    }

    if($('.inp-pasport-date').length) {
        $('.inp-pasport-date').each(function(){
          $(this).mask('99.99.9999');
        })
    }

    if($('.inp-pasport-code').length) {
        $('.inp-pasport-code').each(function(){
          $(this).mask('999-999');
        })
    }

    //форма
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    // фокус поля
    $(document).on('focus', '.inp', function(){
        $(this).removeClass('error');
    });

    $(document).on('click', '.js--file-styled', function(){
        $(this).removeClass('error');
    });

    // отправка формы
    $(document).on('click', '.js-form-submit', function () {
        var block = $(this);

        if(!(block.hasClass('disable'))){
            var form =  $(this).parents('.main-form');
            var errors = false;

            $(form).find('.required').each(function(){
                var val=$(this).prop('value');
                if(val==''){
                    // проверка на заполненность полей с изображениями (аватарка в ЛК и реквизиты)
                    if($(this).hasClass('input-file') || $(this).hasClass('js--file-styled')){
                        if($(this).hasClass('input-file')){
                            $(this).closest('.lk--content__userpick-img').addClass('error');
                        }
                        if($(this).hasClass('js--file-styled')){
                            $(this).closest('.jq-file').addClass('error');
                        }
                    }else{
                        $(this).addClass('error');
                    }
                    errors=true;
                }
                else{
                    if($(this).hasClass('inp-mail')){
                        if(validateEmail(val) == false){
                            $(this).addClass('error');
                            errors=true;
                        }
                    }
                    if($(this).hasClass('inp-pass-repeat')){
                        var pass_repeat = $(this).prop('value');
                        var pass = form.find('.inp-pass').prop('value');
                        if(pass != pass_repeat){
                            form.find('.inp-pass').addClass('error');
                            form.find('.inp-pass-repeat').addClass('error');
                            errors=true;
                        }
                    }
                }
            });

            if(errors == false){
                var button_value = $(form).find('.js-form-submit').text();
                $(form).find('.js-form-submit').text('Подождите...');

                if(form.hasClass('constructor-form')){
                    form.trigger('submit')
                }

                if(form.hasClass('send-file')){
                    var method = form.attr('method');
                    var action = form.attr('action');
                    var data = new FormData(form[0]);

                    $.ajax({
                        type: method,
                        url: action,
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        async: false,
                        success: function(data) {
                            if(form.hasClass('history-form') || form.hasClass('form-message')){
                                if(form.hasClass('history-form')){
                                    var parse_data = jQuery.parseJSON(data);
                                    $('.history--list').empty().append(parse_data.content);
                                    $(form).find('.js-form-submit').text(button_value);
                                }
                                if(form.hasClass('form-message')){
                                    var parse_data = jQuery.parseJSON(data);
                                    $('.message-list').append(parse_data.content);
                                    $(form).find('.js-form-submit').text(button_value);
                                    form.find('.inp').each(function(){
                                        $(this).prop('value','')
                                    });
                                }
                            }
                            else{
                                form.find('.inp').each(function(){
                                    $(this).prop('value','')
                                });
                                $(form).find('.js-form-submit').text(button_value);
                                $(form).find('.js--form-ok').trigger('click');
                            }
                        },
                        error: function(data) {
                            $(form).find('.js-form-submit').text('Ошибка');
                            setTimeout(function() {
                                $(form).find('.js-form-submit').text(button_value);
                            }, 2000);
                        }
                    });
                }else{
                    var method = form.attr('method');
                    var action = form.attr('action');
                    var data = form.serialize();
                    $.ajax({
                        type: method,
                        url: action,
                        data: data,
                        success: function(data) {
                            if(form.hasClass('history-form') || form.hasClass('form-message')){
                                if(form.hasClass('history-form')){
                                    var parse_data = jQuery.parseJSON(data);
                                    $('.history--list').empty().append(parse_data.content);
                                    $(form).find('.js-form-submit').text(button_value);
                                }
                                if(form.hasClass('form-message')){
                                    var parse_data = jQuery.parseJSON(data);
                                    $('.message-list').append(parse_data.content);
                                    $(form).find('.js-form-submit').text(button_value);
                                    form.find('.inp').each(function(){
                                        $(this).prop('value','')
                                    });
                                }
                            }
                            else{
                                form.find('.inp').each(function(){
                                    $(this).prop('value','')
                                });
                                $(form).find('.js-form-submit').text(button_value);
                                $(form).find('.js--form-ok').trigger('click');
                            }
                        },
                        error: function(data) {
                            $(form).find('.js-form-submit').text('Ошибка');
                            setTimeout(function() {
                                $(form).find('.js-form-submit').text(button_value);
                            }, 2000);
                        }
                    });
                }
            }
        }

        return false;
    });

    $(document).on('click', '.js--need-check', function(){
        var block = $(this);
        setTimeout(function(){
            if(block.find('input').prop('checked')){
                block.closest('.main-form').find('.btn').removeClass('disable')
            }
            else{
                block.closest('.main-form').find('.btn').addClass('disable')
            }
        },50)
    });

    // яндекс-карта
    if ($('#map').length) {
       var myMap;
        function init() {
            myMap = new ymaps.Map('map', {
                center: [59.926932,30.307636],
                zoom: 16,
                controls: ['zoomControl']
            });
            myPlacemark = new ymaps.Placemark([59.926932,30.307636], {}, {
                iconLayout: 'default#image',
                iconImageHref: '../../../img/icon--map.png',
                iconImageSize: [15, 21],
                iconImageOffset: [-2, -23]
            });
            myMap.geoObjects.add(myPlacemark);
            myMap.behaviors.disable('scrollZoom');
        }
        ymaps.ready(init);
    }

    // табы

    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');

        if($(this).closest('ul').hasClass('js--requisite-disabled')){
            $('.tabs__content').find('.required').each(function(){
                $(this).toggleClass('required required-free');
            });
            $('.tabs__content.active').find('.required-free').each(function(){
                $(this).toggleClass('required required-free');
            });
        }
    });

    // rating
    if($('.rating').length){
        $('.rating').each(function(){
            var block = $(this);
            block.rating('create',{
                coloron:'#ffd200',
                coloroff: "#d7d7d7"
            });
        });
    };

    // переключение сообщений
    $('ul.chat--list').on('click', 'li:not(.active)', function() {

        var block = $(this);
        var url = block.parents('.chat--list').attr('data-url');
        var id = block.attr('data-user');

        $.ajax({
          url: url,
          method: 'post',
          data: {id: id},
          success: function (data) {
              var parse_data = jQuery.parseJSON(data);
              block.addClass('active').siblings().removeClass('active');
              $('.chat').empty().append(parse_data.content);
              block.find('.chat--list__el-message').remove();
          }
        });
      });

    // закрыть проект
    $(document).on('click', '.js--change-project', function(){
        var block = $(this);
        var container = block.closest('li');
        var url = block.attr('data-url');
        var id = block.attr('data-id');

        $.ajax({
          url: url,
          method: 'post',
          data: {id: id},
          success: function (data) {
            container.remove();
          }
        });

        return false;
    });

    // фильтр в проектах
    $(document).on('change', '.project-filter label', function(){
        var block = $(this);
        var form = block.closest('form');

        if(block.parents('div').hasClass('project-filter__category')){
            if (timeout_link) {
                clearTimeout(timeout_link);
            }
            timeout_link = setTimeout(function () {
                send_filter(form);
            }, 1000);
        }else{
            send_filter(form);
        }
    });

    function send_filter(form){
        var method = form.attr('method');
        var action = form.attr('action');
        var data = form.serialize();
        $.ajax({
            type: method,
            url: action,
            data: data,
            success: function(data) {
                var parse_data = jQuery.parseJSON(data);
                $('.js--load-container').empty().append(parse_data.content);
                $('.fancybox-close').trigger('click');
            }
        });
    }

    // добавить в избранное
    $(document).on('click', '.js--add-favourite', function(){
        var block = $(this);
        var man = block.parents('.general-list__el');
        var url = block.attr('data-url');
        var id = man.attr('data-id');

        $.ajax({
          url: url,
          method: 'post',
          data: {id: id},
          success: function (data) {
            if(man.hasClass('favourite')){
                man.removeClass('favourite');
                if(man.find('.general-list__el__content__links .js--add-favourite').length){
                    man.find('.general-list__el__content__links .js--add-favourite').text(man.find('.general-list__el__content__links .js--add-favourite').attr('data-text-add'))
                }
            }else{
                man.addClass('favourite');
                if(man.find('.general-list__el__content__links .js--add-favourite').length){
                    man.find('.general-list__el__content__links .js--add-favourite').text(man.find('.general-list__el__content__links .js--add-favourite').attr('data-text-remove'))
                }
            }
          }
        });

        return false;
    });

    // отказать исполнителем
    $(document).on('click', '.js--refuse', function(){
        var block = $(this);
        var man = block.parents('.general-list__el');
        var url = block.attr('data-url');
        var id = man.attr('data-id');

        $.ajax({
          url: url,
          method: 'post',
          data: {id: id},
          success: function () {
            man.addClass('refuse').find('.doer-status').text("Вы отказали этому исполнителю");
            if(man.parents('ul').hasClass('has-doer') && man.hasClass('doer')){
                man.parents('ul').removeClass('has-doer')
            }
          }
        });

        return false;
    });

    // сделать исполнителем
    $(document).on('click', '.js--make-doer', function(){
        var block = $(this);
        var man = block.parents('.general-list__el');
        var url = block.attr('data-url');
        var id = man.attr('data-id');
        var index = man.index();
        var ul = man.parents('ul');

        $.ajax({
          url: url,
          method: 'post',
          data: {id: id},
          success: function () {
            man.parents('ul').addClass('has-doer');
            man.addClass('doer').find('.doer-status').text('Исполнитель');
          }
        });

        return false;
    });

     $('.js--date').pickmeup({
        format:'d.m.Y',
        position: 'right',
		hide_on_select: true
    });

    // конструктор
        $('ul.tabs__caption-constructor').on('click', 'li:not(.disable)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
      });

    // сдедующая кнопка
    $(document).on('click','.js--next-step', function(){
        var form =  $('.tabs__content.active');
        var errors = false;

        $(form).find('.required').each(function(){
            var val = $(this).prop('value');
            if(val == ''){
                $(this).addClass('error');
                errors=true;
            }
            else{
                if($(this).hasClass('inp-mail')){
                    if(validateEmail(val) == false){
                        $(this).addClass('error');
                        errors=true;
                    }
                }
            }
        });

        if(errors == false){
            $('.tabs__caption-constructor li.active').next('li').removeClass('disable').trigger('click');
        }

        return false;
    });

    $(document).on('click', '.cupboard__doors-type label.radio', function(){
        $('.cupboard__doors-type').addClass('disable');
        $(this).parents('.cupboard__doors-type').removeClass('disable');
        $('.cupboard__doors-type input.checkbox').removeAttr("disabled");
        $('.cupboard__doors-type.disable input.checkbox').attr("disabled", true);
    })

    $(document).on('click', '.js--up', function(){
        var block = $(this);
        setTimeout(function() {
            if (block.find('.inp-checkbox').prop("checked") == true) {
                $('.js--lift').addClass('required')
            } else {
                $('.js--lift').removeClass('required')
            }

        })
    });

    $(document).on('click', '.js--change-list', function(){
        if($('.towns').hasClass('show')){
           $('.towns').removeClass('show');
            $('.change-list').text($('.change-list').attr('data-hidden'))
        }   else{
            $('.towns').addClass('show');
            $('.change-list').text($('.change-list').attr('data-show'))

        }
        return false;
    })

    // Подгрузка диалога в предложение
    $(document).on('click', '.js--load-dialog', function(){
        var block = $(this);
        var container = block.parents('.general-list__el__content');
        var url = block.attr('data-url');
        var id = block.parents('.general-list__el').attr('data-id');

        $.ajax({
          url: url,
          method: 'post',
          data: {id: id},
          success: function (data) {
            container.append(data);
              block.toggleClass('js--load-dialog js--toggle-dialog').text(block.attr('data-text-hide'));
          }
        });

        return false;
    });

    // скрыть/показать диалог в предложении
    $(document).on('click', '.js--toggle-dialog', function(){
        var block = $(this);
        var chat = block.parents('.general-list__el__content').find('.chat');
        chat.fadeToggle(0);
        block.toggleClass('list-hiddden');
        if(block.hasClass('list-hiddden')){
            block.text(block.attr('data-text-show'))
        }else{
            block.text(block.attr('data-text-hide'))
        }
        return false;
    })

    // ввод только цифр в поле
    $(document).on('keydown', '.js--inp-num', function(e){input_number();});

    $(document).on('input','.js--inp-num', function(){
        block = $(this);
        if(block.data("lastval")!= $(this).val()) {
            var value = $(this).val();
            if(value != ''){
                value = Number(value);
                value = value.toString();
                if(value == 'NaN'){
                    block.prop('value',"")
                }else{
                    block.prop('value',value);
                }
            };
            block.data("lastval", $(this).val());
        };
    });

    var input_number = function(){
        var allow_meta_keys=[86, 67, 65];
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9  || event.keyCode == 27 ||
            ($.inArray(event.keyCode,allow_meta_keys) > -1 && (event.ctrlKey === true ||  event.metaKey === true)) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            }
        }
    };

    // изменилась цена или срок проекта
    $(document).on('click', '.radio--wrap label', function(){
        $('.form-opinion .radio--wrap input').each(function(){
            $(this).prop('checked', false);
        });
        $(this).find('input').prop('checked', true);

        if($(this).hasClass('js--change-project')){
            $('.change-project').removeClass('hidden-block').find('.inp').each(function(){
                $(this).addClass('required');
            })

        }else{
            $('.change-project').addClass('hidden-block').find('.inp').each(function(){
                $(this).removeClass('required');
            })
        }
    })

    // Обязательный чекбокс в реквизитах
    $(document).on('change', '.js--check-agreement', function(){
        var block = $(this);

        if(block.find('input').prop('checked')){
            $('.js-form-submit').removeClass('disable')
        }else{
            $('.js-form-submit').addClass('disable')
        }
    })

    // фикс с надписями в фотографиям маленького формата в конструкторе
    $('.constructor__images').on('click', '.js--file-styled', function(){
        $(this).closest('.empty').removeClass('empty');
    })

    // пагинация списка сообщений
    if($('.chat--list').length){
        var list = $('.chat--list');
        var length = Number(list.find('li').length);
        var list_length = Number(list.data('list-length'));
        var dec_counter = Math.ceil(list.find('li').length / list_length);
        var added_counter = 1;
        var added_counter_length;
        var counter = 0;
        $('.chat--pagination').append('<li class="active">' +
            '<a href="" data-pagination="' + added_counter + '">' + added_counter + '</a>' +
            '</li>'
        )

        for (i = 0; i < length; i++) {
            if(counter < list_length){
                counter++;
            }else{
                counter = 1;
                added_counter ++;
                $('.chat--pagination').append('<li>' +
                    '<a href="" data-pagination="' + (Number(added_counter_length) + 1) + '">' + (Number(added_counter_length) + 1) + '</a>' +
                    '</li>'
                )
            }
            added_counter_length = added_counter.toString();
            list.find('li').eq(i).attr("data-pagination", added_counter_length);
            if(added_counter > 1){
                list.find('li').eq(i).addClass('hidden-block');
            }
        }
    }

    // клик на пагинацию в сообщениях
    $('.chat--pagination').on('click', 'li:not(.active) a', function(){
        var el = $(this);
        var index = el.data('pagination');
        el.closest('li').addClass('active').siblings().removeClass('active');

        $('.chat--list li').each(function(){
            var block = $(this);
            if(block.data('pagination') == index){
                block.removeClass('hidden-block')
            }else{
                block.addClass('hidden-block')
            }
        });
        return false;
    });

    $(document).on('click', '.js--delete-img', function(){
        var block = $(this);
        var ul = block.closest('ul.project__about-images');
        var url = ul.data('url');
        var id = block.closest('li').data('img');

        $.ajax({
          url: url,
          method: 'post',
          data: {id: id},
          success: function (data) {
              block.closest('li').remove();
              if(ul.find('li').length == 0){
                ul.prev('h2').remove();
                ul.remove();
              }
          }
        });
        return false;
    })


    /*
    *   ресайз видео и подложки на главной
     */


    function resizeImg() {

        var w1= $('#index .center').width();
        var h1= $('#index .center').height();
        var k1=w1/h1;
        (k1>k ? $('#index .center img').width(w1).height('auto') : $('#index .center img').height(h1).width('auto'));
    }

    function resizeVideo() {

        var w1= $('#index .center').width();
        var h1= $('#index .center').height();
        var k1=w1/h1;
        var ml=0;
        var mt=0;

        if(k1>k){
            w=w1;
            h=w/k;
            mt=-(h/2);
            $('.video-wrap').css({'top': '50%','left': '0'});
        }
        else{
            h=h1;
            w=h*k;
            ml=-(w/2);
            $('.video-wrap').css({'top': '0','left': '50%'});
        }

        $('#index .video-wrap').css({
            'width':w,
            'height':h,
            'margin-left': ml,
            'margin-top': mt,
            'opacity': '1'
        });
    }

    if($('.video-wrap').length){
        resizeImg();
    }

    if(device.ipad() === true){
        $('.video-wrap').css('display', 'none');
        resizeImg();
        $(window).resize(function(){
            resizeImg();
        });
    }
    else{
        var k=1.777777;
        resizeVideo();

        $(window).resize(function(){
            resizeVideo();
            resizeImg();
        });
    }
})
